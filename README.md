# v-ratio

Vue directive for fixed aspect ratio elements.

## Installation

```console
npm install @jamescoyle/v-ratio
```

### Global Installation

The following installs the directive globaly for your project. This is recommended for most cases as it is the easiest to set up and use throughout your project.

```js
import Vue from 'vue'
import ratio from '@jamescoyle/v-ratio'

Vue.directive('ratio', ratio)
```

### Local Installation

You can also use this directive by attaching it directly to a component. This method is recommended for components which are designed to be portable as it packages the dependency into the component.

```js
import ratio from '@jamescoyle/v-ratio'

export default {
    directives: {
        ratio,
    }
}
```

## Usage

```vue
<div v-ratio="16/9">16:9 element. Set the width and the height will be automatically set.</div>
<div v-ratio:width="16/9">16:9 element. Set the width and the height will be automatically set.</div>
<div v-ratio:height="16/9">16:9 element. Set the height and the width will be automatically set.</div>
```

Note: This directive makes use of the [resize observer](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver). This means that you will likely need to include a polyfill to improve browser support.
