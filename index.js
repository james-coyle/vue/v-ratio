const directive = {
	bind(el, binding) {
		const handler = () => {
			if (binding.argument && binding.argument.toLowerCase() == 'height') el.style.width = el.offsetheight / binding.value + 'px'
			else el.style.height = el.offsetWidth / binding.value + 'px'
		}

		new ResizeObserver(handler).observe(el)
		handler()
	},
}

export default directive
